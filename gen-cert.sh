#!/bin/sh

pwd="$PWD"

mkdir -p build

cd build

openssl req -x509 -newkey rsa:4096 -keyout ca_key.pem -out ca_cert.pem -days 365000 -nodes
chmod 600 ca_cert.pem

cp ca_cert.pem ca_key.pem ../

mkdir -p ../server_certs
cp ca_cert.pem ../server_certs/

cd "$pwd"
