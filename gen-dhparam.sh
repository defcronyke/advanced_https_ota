#!/bin/sh

pwd="$PWD"

mkdir -p build

cd build

openssl dhparam -out dhparam.pem 4096
chmod 600 dhparam.pem

cp dhparam.pem ../

cd "$pwd"
